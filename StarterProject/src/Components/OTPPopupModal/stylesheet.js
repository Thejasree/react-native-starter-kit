import { Dimensions, StyleSheet } from "react-native";
import {
  APP_PRIMARY_BORDER_COLOR,
  APP_SECONDARY_BACKGROUND_COLOR
} from "~/colors";
const { deviceHeight } = Dimensions.get("window");
const styles = StyleSheet.create({
  btnVerify: {
    alignSelf: "center",
    marginTop: 10,
    paddingHorizontal: 25
  },
  modalInput: {
    borderColor: APP_PRIMARY_BORDER_COLOR,
    borderWidth: 1,
    margin: 20,
    padding: 10,
    textAlign: "center"
  },
  otpText: {
    alignSelf: "center",
    fontSize: 20,
    marginBottom: 10,
    marginTop: 20,
    textAlign: "center"
  },
  resendOtp: {
    alignSelf: "center",
    fontSize: 14,
    marginBottom: 10,
    marginTop: 20
  },
  view: {
    backgroundColor: APP_SECONDARY_BACKGROUND_COLOR,
    height: deviceHeight,
    marginLeft: 10,
    marginRight: 10,
    padding: 10
  }
});

export default styles;
