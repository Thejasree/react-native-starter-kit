import React from "react";
import { Platform, TouchableOpacity, View } from "react-native";
import { Text } from "native-base";
import QGButton from "~/UI/QGButton";
import QGHeading from "~/UI/QGHeading";
import QGTextInput from "~/UI/QGTextInput";
import styles from "./stylesheet";
import QGPopupModal from "~/UI/QGPopupModal";
import PropTypes from "prop-types";
import { formValidator } from "~/Utilities/Validators";

class OTPPopupModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      otpForm: {
        otp: {
          title: "OTP",
          value: null,
          type: "default",
          success: false,
          error: false,
          helpText: "",
          validators: {
            required: true,
            length: 6
          }
        }
      }
    };
  }

  static propTypes = {
    onPressVerify: PropTypes.func,
    onPressResend: PropTypes.func
  };

  // on change text input
  handleInput = (value, field) => {
    const { otpForm } = { ...this.state };
    otpForm[field].value = value;
    this.setState({ otpForm: otpForm });
  };

  onPressVerify = () => {
    const { otpForm } = { ...this.state };
    if (!this.validateForm(otpForm)) {
      alert("Invalid OTP Number");
    } else {
      this.props.onPressVerify(otpForm.otp.value);
    }
  };

  validateForm() {
    const { otpForm } = { ...this.state };
    const isValid = formValidator(otpForm);
    return isValid;
  }

  render() {
    return (
      <View>
        <QGPopupModal {...this.props}>
          <View style={styles.view}>
            <QGHeading style={styles.otpText}>
              <Text>Please Enter the OTP Below</Text>
            </QGHeading>
            <QGTextInput
              placeholder={this.state.otpForm["otp"].title}
              keyboardType={
                Platform.OS === "android" ? "phone-pad" : "number-pad"
              }
              returnKeyType="done"
              onChangeText={e => this.handleInput(e, "otp")}
              value={this.state.otpForm["otp"].value}
              success={this.state.otpForm["otp"].success}
              error={this.state.otpForm["otp"].error}
              helperText={this.state.otpForm["otp"].helpText}
              style={styles.modalInput}
            />
            <View>
              <QGButton style={styles.btnVerify} onPress={this.onPressVerify}>
                <Text>VERIFY</Text>
              </QGButton>

              <TouchableOpacity onPress={this.props.onPressResend}>
                <View>
                  <Text style={styles.resendOtp}>Resend OTP</Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </QGPopupModal>
      </View>
    );
  }
}

export default OTPPopupModal;
