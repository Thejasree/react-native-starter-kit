import { StyleSheet } from "react-native";
import { APP_SPINNER_BACKGROUND_COLOR } from "~/colors";

const styles = StyleSheet.create({
  activityIndicatorWrapper: {
    alignItems: "center",
    backgroundColor: APP_SPINNER_BACKGROUND_COLOR,
    borderRadius: 10,
    display: "flex",
    height: 100,
    justifyContent: "space-around",
    width: 100
  },
  modalBackground: {
    alignItems: "center",
    backgroundColor: APP_SPINNER_BACKGROUND_COLOR,
    flex: 1,
    flexDirection: "column",
    justifyContent: "space-around"
  }
});
export default styles;
