import React from "react";
import { ActivityIndicator, Modal, View } from "react-native";
import styles from "./stylesheet";
import PropTypes from "prop-types";

const Spinner = props => {
  //console.log(props);
  const { loading } = props;

  if (loading === true)
    return (
      <Modal
        transparent={true}
        animationType={"none"}
        onRequestClose={() => {
          //console.log("close modal");
        }}
      >
        <View style={styles.modalBackground}>
          <View style={styles.activityIndicatorWrapper}>
            <ActivityIndicator />
          </View>
        </View>
      </Modal>
    );
  return null;
};

Spinner.propTypes = {
  loading: PropTypes.bool
};

export default Spinner;
