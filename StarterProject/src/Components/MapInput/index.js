import React from "react";
import { Dimensions } from "react-native";
import { GooglePlacesAutocomplete } from "react-native-google-places-autocomplete";
import PropTypes from "prop-types";
import {
  COLOR_BLACK,
  TRANSPARENT,
  APP_SECONDARY_BACKGROUND_COLOR,
  COLOR_GREY
} from "~/colors";

const windowSize = Dimensions.get("window");
const deviceWidth = windowSize.width;
const deviceHeight = windowSize.height;

class MapInput extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      shouldDisplayListView: false,
      currentLocation: {
        description: "Current Location",
        geometry: { location: { lat: null, lng: null } }
      }
    };
  }

  componentDidMount() {
    this.getInitialState();
  }

  getInitialState() {
    this.getLocation().then(data => {
      // console.log(data);
      const currLoc = { ...this.state.currentLocation };
      currLoc.geometry.location.lat = data.latitude;
      currLoc.geometry.location.lng = data.longitude;
      this.setState({
        currentLocation: currLoc
      });
    });
    // console.log(this.state.currentLocation);
  }

  getLocation = () => {
    return new Promise((resolve, reject) => {
      navigator.geolocation.getCurrentPosition(
        data => resolve(data.coords),
        err => reject(err)
      );
    });
  };

  notifyChangeHandler = loc => {
    this.props.notifyChange(loc);
    this.props.navigation.navigate("Home", { location: loc });
  };

  render() {
    return (
      <GooglePlacesAutocomplete
        placeholder="Search"
        minLength={1} // minimum length of text to search
        autoFocus={true} //automatically keyboard opens
        returnKeyType={"search"} // Can be left out for default return key https://facebook.github.io/react-native/docs/textinput.html#returnkeytype
        listViewDisplayed={this.state.shouldDisplayListView} // true/false/undefined
        fetchDetails={true}
        renderDescription={row => row.description} // custom description render
        onPress={(data, details) => {
          // 'details' is provided when fetchDetails = true
          this.notifyChangeHandler(details);
        }}
        getDefaultValue={() => ""}
        query={{
          // available options: https://developers.google.com/places/web-service/autocomplete
          key: "AIzaSyCdt4xaJ3POu6gcrgQXKkiKevi-urC8pyc",
          language: "en" // language of the results
          // types: "(cities)" // default: 'geocode'
        }}
        styles={{
          description: {
            borderTopWidth: 0,
            borderBottomWidth: 0,
            fontWeight: "bold",
            opacity: 0.9
          },

          listView: {
            backgroundColor: APP_SECONDARY_BACKGROUND_COLOR,
            height: deviceHeight,
            top: 43,
            position: "absolute",
            width: deviceWidth
          },

          textInput: {
            backgroundColor: COLOR_GREY,
            borderColor: TRANSPARENT,
            borderBottomWidth: 1,
            borderTopWidth: 1,
            borderRadius: 0,
            color: COLOR_BLACK,
            height: 45
          },

          textInputContainer: {
            backgroundColor: APP_SECONDARY_BACKGROUND_COLOR,
            borderTopWidth: 0,
            borderBottomWidth: 0,
            bottom: 8,
            height: 45,
            position: "relative",
            right: 8,
            width: "104%"
          }
        }}
        textInputProps={{
          onFocus: () => this.setState({ shouldDisplayListView: true }),
          onBlur: () => this.setState({ shouldDisplayListView: false })
        }}
        nearbyPlacesAPI="GooglePlacesSearch" // Which API to use: GoogleReverseGeocoding or GooglePlacesSearch
        GoogleReverseGeocodingQuery={
          {
            // available options for GoogleReverseGeocoding API : https://developers.google.com/maps/documentation/geocoding/intro
          }
        }
        GooglePlacesSearchQuery={{
          // available options for GooglePlacesSearch API : https://developers.google.com/places/web-service/search
          rankby: "distance"
          // types: "food"
        }}
        predefinedPlacesAlwaysVisible={true}
        predefinedPlaces={[this.state.currentLocation]}
        // filterReverseGeocodingByTypes={[
        //   "locality",
        //   "administrative_area_level_3"
        // ]} // filter the reverse geocoding results by types - ['locality', 'administrative_area_level_3'] if you want to display only cities
        // debounce={200} // debounce the requests in ms. Set to 0 to remove debounce. By default 0ms.
      />
    );
  }
}

MapInput.propTypes = {
  notifyChange: PropTypes.func,
  navigation: PropTypes.object
};

export default MapInput;
