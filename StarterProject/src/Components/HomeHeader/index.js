import React from "react";
import { View, Image, TouchableOpacity } from "react-native";
import { Header as Head, Left, Right, Body, Icon } from "native-base";
import { ALERT_LOGO, SEARCH_LOGO } from "~/constants";
import PropTypes from "prop-types";
import QGIcon from "~/UI/QGIcon";
import styles from "./stylesheet";
const MENU_ICON = <QGIcon name="menu" size={22} style={styles.colorWhite} />;

const Header = ({ isBack, navigation, search }) => {
  // const onPressSearch = () => {
  //   navigation.navigate("Search");
  // };
  const onPressBack = () => {
    navigation.goBack();
  };
  const onPressHamburger = () => {
    if (navigation.state.routeName === "Home") {
      //navigation.openDrawer();
    }
    navigation.goBack();
  };

  return (
    <View>
      <Head transparent style={styles.head}>
        <Left>
          {isBack ? (
            <TouchableOpacity
              onPress={onPressBack}
              style={styles.backIconWidth}
            >
              <Icon
                name="md-arrow-back"
                ios="ios-arrow-back"
                android="md-arrow-back"
                style={styles.back}
              />
            </TouchableOpacity>
          ) : MENU_ICON ? (
            <TouchableOpacity
              onPress={onPressHamburger}
              style={styles.marginStyle}
            >
              {MENU_ICON}
            </TouchableOpacity>
          ) : (
            ""
          )}
        </Left>
        <Body style={styles.flexRow} />
        <Right>
          {!search ? null : SEARCH_LOGO ? (
            <View style={styles.headerRightStyle}>
              <View style={styles.searchView}>
                <Image source={SEARCH_LOGO} style={styles.logoStyle} />
              </View>
              <View style={styles.cartView}>
                <Image source={ALERT_LOGO} style={styles.imageStyle} />
              </View>
            </View>
          ) : (
            ""
          )}
        </Right>
      </Head>
    </View>
  );
};

Header.propTypes = {
  navigation: PropTypes.object,
  isBack: PropTypes.bool,
  search: PropTypes.bool
};

export default Header;
