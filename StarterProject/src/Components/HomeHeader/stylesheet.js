import { StyleSheet } from "react-native";
import {
  APP_HEADER_TEXT_COLOR,
  BACKGROUND_COLOR_ORANGE,
  APP_SECONDARY_BACKGROUND_COLOR
} from "~/colors";

const styles = StyleSheet.create({
  back: {
    color: APP_SECONDARY_BACKGROUND_COLOR,
    fontSize: 30,
    fontWeight: "bold",
    marginTop: 5
  },
  backIconWidth: {
    width: 25
  },
  cartView: {
    padding: 5,
    position: "relative"
  },
  colorWhite: {
    color: APP_HEADER_TEXT_COLOR
  },
  flexRow: {
    flexDirection: "row"
  },
  head: {
    backgroundColor: BACKGROUND_COLOR_ORANGE,
    //borderBottomColor: APP_SECONDARY_BACKGROUND_COLOR,
    //borderBottomWidth: 2,
    // marginTop: Platform.OS === "android" ? 0 : 0,
    paddingBottom: 10
  },
  headerRightStyle: {
    flexDirection: "row",
    marginRight: 5
    //marginTop: 10
  },
  imageStyle: {
    height: 26,
    width: 26
  },
  logoStyle: {
    height: 26,
    justifyContent: "flex-end",
    marginRight: 2,
    width: 26
  },
  mainContainer: {
    flex: 1
  },
  marginStyle: {
    marginLeft: 5
    // marginTop: 10
  },
  searchView: {
    marginLeft: 4,
    marginTop: 6
  }
});

export default styles;
