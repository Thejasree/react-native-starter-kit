import React, { Component } from "react";
import { View } from "react-native";
import MapView from "react-native-maps";
import PropTypes from "prop-types";
import { MAP_CIRCLE_STROKE_COLOR } from "~/colors";

class MapCircle extends Component {
  constructor(props) {
    super(props);
    //console.log(props.coordinate, props.radius)
  }

  render() {
    return (
      <View>
        <MapView.Circle
          center={this.props.coordinate}
          radius={this.props.radius}
          strokeWidth={2}
          strokeColor={MAP_CIRCLE_STROKE_COLOR}
          //fillColor="rgba(0,0,0,0.2)"
        />
      </View>
    );
  }
}

MapCircle.propTypes = {
  radius: PropTypes.number,
  coordinate: PropTypes.object
};
export default MapCircle;
