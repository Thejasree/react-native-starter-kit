import { StyleSheet } from "react-native";
import { APP_SECONDARY_BACKGROUND_COLOR, COLOR_2a2f43 } from "~/colors";
const styles = StyleSheet.create({
  body: {
    padding: 10,
    paddingTop: 0
  },
  button: {},
  buttonImage: {
    height: 25,
    width: 30
  },
  container: {
    backgroundColor: APP_SECONDARY_BACKGROUND_COLOR,
    overflow: "hidden"
  },
  title: {
    color: COLOR_2a2f43,
    flex: 1,
    paddingRight: 10,
    paddingTop: 5
  },
  titleContainer: {
    flexDirection: "row"
  }
});

export default styles;
