import React, { Component } from "react";
import { Animated, Image, Text, TouchableHighlight, View } from "react-native";
import { TODOWN_LOGO, TOTOP_LOGO } from "~/constants";
import styles from "./stylesheet";
import PropTypes from "prop-types";

class Panel extends Component {
  static propTypes = {
    children: PropTypes.object,
    title: PropTypes.string,
    sendExpandedValue: PropTypes.func
  };

  constructor(props) {
    super(props);
    //console.log(this.props);
    this.icons = {
      //Step 2
      up: TODOWN_LOGO,
      down: TOTOP_LOGO
    };

    this.state = {
      //Step 3
      title: props.title,
      expanded: false,
      animation: new Animated.Value()
    };
  }

  toggle() {
    this.props.sendExpandedValue(!this.state.expanded);
    this.setState({
      expanded: !this.state.expanded //Step 2
    });
    //Step 1
    let initialValue = this.state.expanded
        ? this.state.maxHeight + this.state.minHeight
        : this.state.minHeight,
      finalValue = this.state.expanded
        ? this.state.minHeight + 5
        : this.state.maxHeight + this.state.minHeight + 10;
    // console.log(initialValue, finalValue);

    this.setState({
      expanded: !this.state.expanded //Step 2
    });

    this.state.animation.setValue(initialValue); //Step 3
    Animated.spring(
      //Step 4
      this.state.animation,
      {
        toValue: finalValue
      }
    ).start(); //Step 5
  }

  //set MAX height
  setMaxHeight(event) {
    this.setState({
      maxHeight: event.nativeEvent.layout.height
    });
  }

  //set MIN Height
  setMinHeight(event) {
    this.setState({
      minHeight: event.nativeEvent.layout.height
    });
  }

  render() {
    let icon;
    if (this.state.title) {
      icon = this.icons["up"];
    }

    if (this.state.expanded && this.state.title) {
      icon = this.icons["down"]; //Step 4
    }

    //Step 5
    return (
      <Animated.View
        style={[styles.container, { height: this.state.animation }]}
      >
        <TouchableHighlight
          style={styles.button}
          onPress={this.toggle.bind(this)}
          underlayColor="#f1f1f1"
        >
          <View
            style={styles.titleContainer}
            onLayout={this.setMinHeight.bind(this)}
          >
            <Text style={styles.title}>{this.state.title}</Text>

            <Image style={styles.buttonImage} source={icon} />
          </View>
        </TouchableHighlight>
        {this.state.expanded && (
          <View style={styles.body} onLayout={this.setMaxHeight.bind(this)}>
            {this.props.children}
          </View>
        )}
      </Animated.View>
    );
  }
}

export default Panel;
