import React from "react";
import { Platform, View, Text } from "react-native";
import QGButton from "~/UI/QGButton";
import QGTextInput from "~/UI/QGTextInput";
import QGInstructions from "~/UI/QGInstructions";
import styles from "./stylesheet";
import QGPopupModal from "~/UI/QGPopupModal";
import QGText from "~/UI/QGText";
import PropTypes from "prop-types";

class PasswordPopupModal extends React.Component {
  static propTypes = {
    onPressSubmit: PropTypes.func
  };

  render() {
    return (
      <View>
        <QGPopupModal {...this.props}>
          <View style={styles.view}>
            <QGInstructions>
              <Text>Please check your phone number</Text>
            </QGInstructions>
            <QGTextInput
              keyboardType={
                Platform.OS === "android" ? "phone-pad" : "number-pad"
              }
              returnKeyType="done"
              style={styles.modalInput}
            />
            <View>
              {/*submit button*/}
              <QGButton onPress={this.props.onPressSubmit}>
                <QGText>
                  <Text>SUBMIT</Text>
                </QGText>
              </QGButton>
            </View>
          </View>
        </QGPopupModal>
      </View>
    );
  }
}
export default PasswordPopupModal;
