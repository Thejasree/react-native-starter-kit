import { Dimensions, StyleSheet } from "react-native";
import {
  TRANSPARENT,
  APP_PRIMARY_BORDER_COLOR,
  APP_SECONDARY_BACKGROUND_COLOR
} from "~/colors";
const { deviceHeight } = Dimensions.get("window");
const styles = StyleSheet.create({
  modal: {
    backgroundColor: TRANSPARENT,
    borderRadius: 2
  },
  modalInput: {
    borderColor: APP_PRIMARY_BORDER_COLOR,
    borderWidth: 1,
    margin: 10,
    padding: 10,
    textAlign: "center"
  },
  otpText: {
    alignSelf: "center",
    fontSize: 18,
    marginBottom: 10,
    marginTop: 20,
    textAlign: "center"
  },
  view: {
    backgroundColor: APP_SECONDARY_BACKGROUND_COLOR,
    height: deviceHeight,
    padding: 20
  }
});

export default styles;
