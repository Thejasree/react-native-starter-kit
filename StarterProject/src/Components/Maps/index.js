import React, { Component } from "react";
import { View } from "react-native";
import MapView, { PROVIDER_GOOGLE, Marker } from "react-native-maps";
import styles from "./stylesheet";
import PropTypes from "prop-types";
import MapCircle from "../MapCircle/index";

class Maps extends Component {
  mapStyle = [
    {
      featureType: "landscape",
      stylers: [
        {
          hue: "#FFBB00"
        },
        {
          saturation: 43.400000000000006
        },
        {
          lightness: 37.599999999999994
        },
        {
          gamma: 1
        }
      ]
    },
    {
      featureType: "road.highway",
      stylers: [
        {
          hue: "#FFC200"
        },
        {
          saturation: -61.8
        },
        {
          lightness: 45.599999999999994
        },
        {
          gamma: 1
        }
      ]
    },
    {
      featureType: "road.arterial",
      stylers: [
        {
          hue: "#FF0300"
        },
        {
          saturation: -100
        },
        {
          lightness: 51.19999999999999
        },
        {
          gamma: 1
        }
      ]
    },
    {
      featureType: "road.local",
      stylers: [
        {
          hue: "#FF0300"
        },
        {
          saturation: -100
        },
        {
          lightness: 52
        },
        {
          gamma: 1
        }
      ]
    },
    {
      featureType: "water",
      stylers: [
        {
          hue: "#0078FF"
        },
        {
          saturation: -13.200000000000003
        },
        {
          lightness: 2.4000000000000057
        },
        {
          gamma: 1
        }
      ]
    },
    {
      featureType: "poi",
      stylers: [
        {
          hue: "#00FF6A"
        },
        {
          saturation: -1.0989010989011234
        },
        {
          lightness: 11.200000000000017
        },
        {
          gamma: 1
        }
      ]
    }
  ];

  constructor(props) {
    super(props);
    this.state = { flex: 0.99 };
  }

  componentDidMount() {
    setTimeout(() => this.setState({ flex: 1 }), 500);
  }

  render() {
    return (
      <View style={styles.container}>
        {/*Map View*/}
        <MapView
          provider={PROVIDER_GOOGLE}
          style={[{ flex: this.state.flex }, this.props.style]}
          showsUserLocation={true}
          showsMyLocationButton={true}
          followsUserLocation={true}
          // showsCompass={true}
          cacheEnabled={false}
          loadingEnabled={true}
          toolbarEnabled={false}
          customMapStyle={this.mapStyle}
          initialRegion={this.props.initialRegion}
          region={this.props.region}
          onPress={this.props.onPress}
          // onRegionChangeComplete={reg => this.props.onRegionChange(reg)}
        >
          <MapCircle radius={4828} coordinate={this.props.currentLocation} />
          <MapCircle radius={8046} coordinate={this.props.currentLocation} />
          <Marker coordinate={this.props.currentLocation} />
        </MapView>
      </View>
    );
  }
}

// prop types
Maps.propTypes = {
  mapMarkers: PropTypes.array,
  region: PropTypes.object,
  currentLocation: PropTypes.object,
  initialRegion: PropTypes.object,
  onRegionChange: PropTypes.func,
  onPress: PropTypes.func,
  style: PropTypes.object,
  children: PropTypes.array
};
export default Maps;
