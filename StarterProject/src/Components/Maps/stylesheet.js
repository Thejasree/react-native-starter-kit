import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  mapMarker: { height: 40, width: 40 },
  maps: {
    flex: 1
  }
});

export default styles;
