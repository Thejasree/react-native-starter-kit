import React from "react";
import { Slider, Text, View } from "react-native";
import { styles } from "./stylesheet";
import { SLIDER_THUMB } from "~/colors";
import PropTypes from "prop-types";

const SliderComponent = props => {
  return (
    <View style={styles.container}>
      <View style={styles.flexStyle}>
        <View style={styles.overflowStyle}>
          <Slider
            style={styles.sliderStyle}
            step={1}
            minimumValue={props.sliderProps.minValue}
            maximumValue={props.sliderProps.maxValue}
            value={props.sliderProps.value}
            onValueChange={props.sliderMove}
            maximumTrackTintColor={SLIDER_THUMB}
            minimumTrackTintColor={props.sliderProps.sliderColor}
            thumbTintColor={SLIDER_THUMB}
          />
          <View style={styles.textCon}>
            <Text style={styles.colorGrey}>
              {props.sliderProps.sliderValue}
            </Text>
          </View>
        </View>
      </View>
    </View>
  );
};
SliderComponent.propTypes = {
  sliderProps: PropTypes.object,
  sliderMove: PropTypes.func
};
export default SliderComponent;
