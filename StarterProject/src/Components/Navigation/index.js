import React, { Component } from "react";
import {
  createAppContainer,
  createStackNavigator,
  createSwitchNavigator
} from "react-navigation";
import PropTypes from "prop-types";
import WelcomeScreen from "~/Screens/Welcome";
import RegisterScreen from "~/Screens/Register";
import HomeScreen from "~/Screens/Home";

const AuthStack = createStackNavigator({
  Welcome: {
    screen: WelcomeScreen
  },
  Register: {
    screen: RegisterScreen
  }
});
const AppStack = createStackNavigator({
  Home: {
    screen: HomeScreen
  }
});

// switch navigator for sign up and Home Screens
const AppNavigator = createSwitchNavigator(
  {
    Auth: AuthStack,
    App: AppStack
  },
  {
    initialRouteName: "Auth"
  }
);

//create app container for app navigator
const AppContainer = createAppContainer(AppNavigator);

// export default createAppContainer(AppNavigator);

class Navigation extends Component {
  componentDidMount = () => {};

  constructor(props) {
    super(props);
  }

  render() {
    return <AppContainer />;
  }
}
Navigation.propTypes = {
  navigation: PropTypes.object,
  profileDetails: PropTypes.object
};
export default Navigation;
