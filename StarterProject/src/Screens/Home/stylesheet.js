import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    flex: 1,
    justifyContent: "center",
    marginTop: 50
  },
  mainContainer: {
    flex: 1
  }
});
export default styles;
