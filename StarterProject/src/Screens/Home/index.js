import React, { Component } from "react";
import { Text, View } from "react-native";
import styles from "./stylesheet";
import { Container } from "native-base";
import Header from "~/Components/HomeHeader";
import PropTypes from "prop-types";

class HomeScreen extends Component {
  constructor(props) {
    super(props);
  }
  static navigationOptions = {
    header: null
  };
  render() {
    return (
      <Container style={styles.mainContainer}>
        <Header navigation={this.props.navigation} search={true} />
        <View style={styles.container}>
          <Text>Welcome to Home Screen</Text>
        </View>
      </Container>
    );
  }
}
HomeScreen.propTypes = {
  navigation: PropTypes.object
};
export default HomeScreen;
