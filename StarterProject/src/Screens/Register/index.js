import React, { Component } from "react";
import { Button, Text, View } from "react-native";
import styles from "./stylesheet";
import PropTypes from "prop-types";

class RegisterScreen extends Component {
  constructor(props) {
    super(props);
  }
  static navigationOptions = {
    headerTitle: "Register"
  };
  onPressButton = () => {
    this.props.navigation.navigate("Home");
  };
  render() {
    return (
      <View style={styles.container}>
        <Text>Welcome to Register Screen</Text>
        <Button title="CLICK ME" onPress={this.onPressButton} />
      </View>
    );
  }
}

RegisterScreen.propTypes = {
  navigation: PropTypes.object
};
export default RegisterScreen;
