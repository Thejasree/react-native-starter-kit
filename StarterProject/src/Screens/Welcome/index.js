import React, { Component } from "react";
import { Text, View, Button } from "react-native";
import styles from "./stylesheet";
import PropTypes from "prop-types";

class WelcomeScreen extends Component {
  constructor(props) {
    super(props);
  }
  static navigationOptions = {
    headerTitle: "Welcome"
  };
  onPressButton = () => {
    this.props.navigation.navigate("Register");
  };
  render() {
    return (
      <View style={styles.container}>
        <Text>Welcome to Welcome Screen</Text>
        <Button title="CLICK ME" onPress={this.onPressButton} />
      </View>
    );
  }
}
WelcomeScreen.propTypes = {
  navigation: PropTypes.object
};
export default WelcomeScreen;
