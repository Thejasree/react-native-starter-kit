import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  heading: {
    alignSelf: "center",
    fontSize: 16,
    fontWeight: "bold",
    margin: 10,
    marginBottom: 10,
    marginTop: 20,
    position: "relative",
    textAlign: "center"
  }
});

export default styles;
