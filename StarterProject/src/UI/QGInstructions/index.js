import React from "react";

import { Text } from "react-native";
import styles from "./stylesheet";
import PropType from "prop-types";

const QGInstructions = props => {
  return (
    <Text style={[styles.heading, props.style]} {...props}>
      {props.children}
    </Text>
  );
};

QGInstructions.propTypes = {
  style: PropType.object,
  children: PropType.object
};

export default QGInstructions;
