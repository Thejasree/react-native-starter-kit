import { StyleSheet } from "react-native";
import { APP_PRIMARY_COLOR, APP_HEADER_TEXT_COLOR } from "~/colors";

const styles = StyleSheet.create({
  header: {
    alignItems: "flex-start",
    backgroundColor: APP_PRIMARY_COLOR,
    // borderBottomColor: APP_SECONDARY_BACKGROUND_COLOR,
    //borderBottomWidth: 2,
    justifyContent: "flex-start"
  },

  icon: {
    color: APP_HEADER_TEXT_COLOR,
    marginTop: 10
  },

  textHeader: {
    color: APP_HEADER_TEXT_COLOR,
    fontFamily: "Roboto",
    fontSize: 18,
    fontWeight: "bold",
    marginTop: 12,
    paddingLeft: 10,
    textAlign: "center"
  }
});

export default styles;
