import { Header } from "native-base";
import React from "react";
import styles from "./stylesheet";
import PropTypes from "prop-types";
import { Text, TouchableOpacity } from "react-native";
import QGIcon from "../QGIcon";
import QGText from "../QGText";

const QGNavigationHeader = props => {
  return (
    <Header style={[styles.header, props.style]}>
      <TouchableOpacity onPress={props.onPressDetails}>
        <QGIcon name="menu" style={styles.icon} />
      </TouchableOpacity>
      <QGText style={styles.textHeader}>
        <Text>{props.title}</Text>
      </QGText>
    </Header>
  );
};

QGNavigationHeader.propTypes = {
  style: PropTypes.object,
  title: PropTypes.string,
  onPressDetails: PropTypes.func
};

export default QGNavigationHeader;
