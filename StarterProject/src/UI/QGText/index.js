import React from "react";
import { Text } from "react-native";
import PropTypes from "prop-types";
import styles from "./stylesheet";

const QGText = props => {
  return (
    <Text style={[styles.text, props.style]} {...props}>
      {props.children}
    </Text>
  );
};

QGText.propTypes = {
  style: PropTypes.object,
  children: PropTypes.object
};

export default QGText;
