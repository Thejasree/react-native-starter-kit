import { StyleSheet } from "react-native";
import { APP_HEADER_TEXT_COLOR } from "~/colors";

const styles = StyleSheet.create({
  text: {
    color: APP_HEADER_TEXT_COLOR,
    fontFamily: "Roboto"
  }
});

export default styles;
