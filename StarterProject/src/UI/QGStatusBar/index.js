import React from "react";
import { StatusBar } from "react-native";
import PropTypes from "prop-types";
import styles from "./stylesheet";

const QGStatusBar = () => {
  return <StatusBar barStyle="light-content" style={[styles.color]} />;
};
export default QGStatusBar;

QGStatusBar.prototype = {
  style: PropTypes.object
};
