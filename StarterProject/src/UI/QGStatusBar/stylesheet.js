import { StyleSheet } from "react-native";
import { APP_PRIMARY_BORDER_COLOR } from "~/colors";

const styles = StyleSheet.create({
  color: {
    backgroundColor: APP_PRIMARY_BORDER_COLOR
  }
});
export default styles;
