import { StyleSheet } from "react-native";
import { COLOR_GREY } from "~/colors";

export const styles = StyleSheet.create({
  textArea: {
    height: 100,
    justifyContent: "flex-start",
    textAlignVertical: "top"
  },

  textAreaContainer: {
    backgroundColor: COLOR_GREY,
    borderColor: COLOR_GREY,
    borderRadius: 15,
    marginBottom: 15,
    marginLeft: 20,
    marginRight: 20,
    marginTop: 15,
    padding: 5
  }
});
