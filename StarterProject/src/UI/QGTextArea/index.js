import React from "react";
import { TextInput, View } from "react-native";
import { styles } from "./stylesheet";
import PropTypes from "prop-types";

const QGTextArea = props => {
  return (
    <View style={styles.textAreaContainer}>
      <TextInput
        onChangeText={props.remarksHandler}
        value={props.remarks}
        style={styles.textArea}
        underlineColorAndroid="transparent"
        placeholder="Add comments"
        placeholderTextColor="grey"
        multiline={true}
      />
    </View>
  );
};

QGTextArea.propTypes = {
  remarksHandler: PropTypes.func,
  remarks: PropTypes.string
};

export default QGTextArea;
