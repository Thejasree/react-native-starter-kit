import React from "react";
import { TextInput, View, Text } from "react-native";
import PropTypes from "prop-types";
import styles from "./stylesheet";
import QGText from "~/UI/QGText";

const QGTextInput = props => {
  const { helperText } = props;
  return (
    <View style={styles.textInput}>
      <TextInput
        underlineColorAndroid="transparent"
        placeholderTextColor="grey"
        autoCapitalize="none"
        style={[styles.input, props.style]}
        {...props}
      />
      <QGText>
        <Text style={styles.textStyle}>{helperText}</Text>
      </QGText>
    </View>
  );
};

QGTextInput.propTypes = {
  style: PropTypes.object,
  helperText: PropTypes.string
};

export default QGTextInput;
