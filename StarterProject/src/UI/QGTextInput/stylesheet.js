import { Platform, StyleSheet } from "react-native";
import { APP_PRIMARY_BORDER_COLOR, COLOR_ALERT } from "~/colors";

const styles = StyleSheet.create({
  input: {
    borderBottomWidth: 1,
    borderColor: APP_PRIMARY_BORDER_COLOR,
    fontSize: 14,
    height: 50,
    margin: Platform.OS === "android" ? 0 : 5,
    // marginLeft: 10,
    // marginRight: 10,
    padding: Platform.OS === "android" ? 10 : 10,
    paddingBottom: 10
  },
  textInput: {
    marginLeft: 10,
    marginRight: 10
  },
  textStyle: {
    color: COLOR_ALERT
  }
});

export default styles;
