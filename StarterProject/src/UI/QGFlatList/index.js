import React from "react";
import { FlatList, Text, View } from "react-native";
import styles from "./stylesheet";
import PropTypes from "prop-types";

export const QGFlatList = props => {
  //console.log(props);
  return (
    <FlatList
      data={props.data}
      renderItem={props.renderItem}
      ListEmptyComponent={
        <View style={styles.noCategoriesView}>
          <Text style={styles.noCategories}>No Categories</Text>
        </View>
      }
      keyExtractor={props.keyExtractor}
    />
  );
};

QGFlatList.propTypes = {
  data: PropTypes.array,
  renderItem: PropTypes.func,
  keyExtractor: PropTypes.func
};
