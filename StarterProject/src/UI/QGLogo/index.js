import React from "react";
import { Image } from "react-native";
import styles from "./stylesheet";
import PropTypes from "prop-types";

const QGLogo = props => {
  return <Image style={[styles.img, props.style]} {...props} />;
};

QGLogo.propTypes = {
  style: PropTypes.object
};

export default QGLogo;
