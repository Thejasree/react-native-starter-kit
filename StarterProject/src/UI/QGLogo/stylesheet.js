import { Dimensions, Platform, StyleSheet } from "react-native";

const { height, width } = Dimensions.get("window");
const styles = StyleSheet.create({
  img: {
    height: Platform.OS === "android" ? height / 6 : height / 4.5,
    width: Platform.OS === "android" ? width / 4 : width / 3
  }
});

export default styles;
