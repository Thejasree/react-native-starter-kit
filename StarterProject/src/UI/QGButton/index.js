import React from "react";
import { Button } from "native-base";
import styles from "./stylesheet";
import PropTypes from "prop-types";

const QGButton = props => {
  return (
    <Button rounded warning style={[styles.button, props.style]} {...props}>
      {props.children}
    </Button>
  );
};

QGButton.propTypes = {
  style: PropTypes.object,
  children: PropTypes.object
};

export default QGButton;
