import { StyleSheet } from "react-native";
import { APP_PRIMARY_COLOR } from "~/colors";

const styles = StyleSheet.create({
  icon: {
    color: APP_PRIMARY_COLOR,
    fontSize: 18
  }
});

export default styles;
