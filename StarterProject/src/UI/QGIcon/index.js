import React from "react";
import styles from "./stylesheet";
import { Icon } from "native-base";
import PropTypes from "prop-types";

const QGIcon = props => {
  return (
    <Icon name={props.name} style={[styles.icon, props.style]} {...props} />
  );
};

QGIcon.propTypes = {
  name: PropTypes.string,
  style: PropTypes.object
};

export default QGIcon;
