import { Dimensions, Platform, StyleSheet } from "react-native";
import { APP_PRIMARY_BORDER_COLOR, COLOR_ALERT } from "~/colors";

const { deviceWidth } = Dimensions.get("window");

const styles = StyleSheet.create({
  container: {
    marginLeft: 10,
    marginRight: 10
  },
  eye: {
    padding: 5,
    position: "absolute",
    right: 15,
    top: Platform.OS === "android" ? 0 : 0,
    zIndex: 1
  },
  textInput: {
    flex: 1,
    fontSize: 14,
    //height: 50,
    marginBottom: Platform.OS === "android" ? 0 : 10,
    marginLeft: 5,
    paddingLeft: Platform.OS === "android" ? 5 : 5,
    paddingRight: Platform.OS === "android" ? 5 : 10,
    paddingTop: Platform.OS === "android" ? 0 : 10,
    width: deviceWidth
  },
  textInputView: {
    borderBottomWidth: 1,
    borderColor: APP_PRIMARY_BORDER_COLOR,
    flexDirection: "row",
    //paddingBottom: 5,
    marginBottom: Platform.OS === "android" ? 5 : 10,
    // marginTop: 10,
    // marginLeft: 10,
    // marginRight: 10,
    marginTop: 10
  },
  textStyle: {
    color: COLOR_ALERT
  }
});
export default styles;
