import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  logo: {
    height: 45,
    marginLeft: 5,
    marginRight: 5,
    width: 45
  }
});

export default styles;
