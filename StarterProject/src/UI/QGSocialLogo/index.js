import React from "react";
import { Image } from "react-native";
import PropTypes from "prop-types";
import styles from "./stylesheet";

const QGSocialLogo = props => {
  return <Image style={[styles.logo, props.style]} {...props} />;
};

QGSocialLogo.propTypes = {
  style: PropTypes.object
};

export default QGSocialLogo;
