export const APP_SECONDARY_BACKGROUND_COLOR = "#FFFFFF";

export const BACKGROUND_COLOR_ORANGE = "#FFA500";

export const APP_FLATLIST_NOCATEGORIES_COLOR = "#666666";

export const APP_SPINNER_BACKGROUND_COLOR = "#00000040";

export const TRANSPARENT = "transparent";

export const APP_PRIMARY_BORDER_COLOR = "#808080";

export const APP_FLATLIST_SEPARATOR_COLOR = "#000";

export const APP_TEXT_SECONDARY_COLOR = "#808080";

export const APP_HEADER_TEXT_COLOR = "#FFFFFF";

export const COLOR_RED = "#FF0000";

export const COLOR_GREY = "#D3D3D3";

export const COLOR_BLACK = "#000";

export const COLOR_YELLOW = "#ffb45f";

export const APP_PRIMARY_COLOR = "#FFA500";

export const HOME_HEADER_TINT_COLOR = "#4169e1";

export const SLIDER_THUMB = "#696969";

export const SLIDER_LOW = "#FFD700";

export const SLIDER_MEDIUM = "#FFA500";

export const SLIDER_HIGH = "#FF4500";

export const LOCATION_SEARCH = "#FFB900";

export const COLOR_DARK_GREY = "#696969";

export const COLOR_ALERT = "#ff0000";

export const MAP_CIRCLE_STROKE_COLOR = "#b6e0ed";

export const MAP_CIRCLE_FILLED_COLOR = "#dce9ed";
