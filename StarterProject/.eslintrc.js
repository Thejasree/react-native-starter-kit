module.exports = {
    parser: "babel-eslint",
    env: {
        es6: true,
        node: true,
        browser: true,
	    "react-native/react-native": true
    },
    parserOptions: {
        ecmaVersion: 6,
        sourceType: "module",
        ecmaFeatures: {
            jsx: true
        }
    },
    plugins: ["react", "react-native"],
    extends: [
        "eslint:recommended",
        "plugin:react/recommended",
        "plugin:prettier/recommended",
        "prettier",
	    "plugin:react-native/all"
    ],
    settings : {
        react : {
            version : '^16.6'
        },
        rules: {
            "react/no-string-refs": "off",
            "indent": {
                "options": ["spaces", 2]
            }
        }
    }
};
